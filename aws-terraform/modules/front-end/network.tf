resource "aws_vpc" "apopx_vpc" {
  cidr_block       = "10.1.0.0/16"
  instance_tenancy = "default"
}

resource "aws_subnet" "pubsubnet" {
  vpc_id                  = "${aws_vpc.apopx_vpc.id}"
  cidr_block              = "10.1.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "${var.subnet_zone}"
}

resource "aws_subnet" "privsubnet" {
  vpc_id            = "${aws_vpc.apopx_vpc.id}"
  cidr_block        = "10.1.0.0/24"
  availability_zone = "${var.subnet_zone}"
  tags {
    Name = "front_end_privsubnet1"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.apopx_vpc.id}"

}
