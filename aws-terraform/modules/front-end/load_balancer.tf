resource "aws_elb" "frontend_lb" {
  name               = "frontend-lb"
  internal           = false
  subnets            = ["${aws_subnet.pubsubnet.id}"]
  security_groups    = ["${aws_security_group.apopx_nsg.id}"]
  instances = ["${module.rproxy01.rproxy-1_instance}", "${module.rproxy02.rproxy-2_instance}"]   

 listener {
      instance_port = 80
      instance_protocol = "http"
      lb_port    = 80
      lb_protocol = "http"
   }

   health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }
}

