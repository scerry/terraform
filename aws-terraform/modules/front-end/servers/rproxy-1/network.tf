resource "aws_network_interface" "rproxy-1_ni" {
  subnet_id   = "${var.pubsubnet}"
  private_ips = ["10.1.1.8"]
  security_groups = ["${var.nsg}"]

  tags = {
    Name = "primary_network_interface"
  }
}
