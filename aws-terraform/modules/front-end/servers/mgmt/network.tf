resource "aws_network_interface" "mgmt_ni" {
  subnet_id   = "${var.pubsubnet}"
  private_ips = ["10.1.1.7"]
  security_groups = ["${var.nsg}"]

  tags = {
    Name = "primary_network_interface"
  }
}
