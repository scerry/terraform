resource "aws_instance" "mgmt" {
  ami = "${var.redhat}"
  instance_type = "t2.micro"
  key_name = "pubkey"
  
    network_interface {
    network_interface_id = "${aws_network_interface.mgmt_ni.id}"
    device_index         = 0
  }

  }

resource "aws_volume_attachment" "attachment" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.volume.id}"
  instance_id = "${aws_instance.mgmt.id}"
}

resource "aws_ebs_volume" "volume" {
  availability_zone = "eu-west-2b"
  size              = 10
}
