resource "aws_network_interface" "rproxy-2_ni" {
  subnet_id   = "${var.pubsubnet}"
  private_ips = ["10.1.1.9"]
  security_groups = ["${var.nsg}"]

  tags = {
    Name = "primary_network_interface"
  }
}

