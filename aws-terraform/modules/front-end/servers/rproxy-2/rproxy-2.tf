resource "aws_instance" "rproxy-2" {
  ami = "${var.redhat}"
  instance_type = "t2.micro"
  key_name = "pubkey"

   network_interface {
    network_interface_id = "${aws_network_interface.rproxy-2_ni.id}"
    device_index         = 0
  }
  }

resource "aws_volume_attachment" "attachment" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.volume.id}"
  instance_id = "${aws_instance.rproxy-2.id}"
}

resource "aws_ebs_volume" "volume" {
  availability_zone = "eu-west-2b"
  size              = 10
}
