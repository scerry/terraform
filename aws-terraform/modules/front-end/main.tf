module "rproxy01" {
     source = "./servers/rproxy-1"
     pubsubnet = "${aws_subnet.pubsubnet.id}"
     security_group = "${aws_security_group.apopx_nsg.id}"
     pubkey = "{var.pubkey}"
     zone = "{var.zone}"
     ssh_key = "{var.ssh_key}"
     redhat = "${var.redhat}"
     privsubnet = "${aws_subnet.privsubnet.id}"
     nsg = "${aws_security_group.apopx_nsg.id}"
}

module "rproxy02" {
     source = "./servers/rproxy-2"
     pubsubnet = "${aws_subnet.pubsubnet.id}"
     security_group = "${aws_security_group.apopx_nsg.id}"
     pubkey = "{var.pubkey}"
     zone = "{var.zone}"
     ssh_key = "{var.ssh_key}"
     redhat = "${var.redhat}"
     privsubnet = "${aws_subnet.privsubnet.id}"
     nsg = "${aws_security_group.apopx_nsg.id}"
}

module "mgmt" {
     source = "./servers/mgmt"
     pubsubnet = "${aws_subnet.pubsubnet.id}"
     security_group = "${aws_security_group.apopx_nsg.id}"
     pubkey = "{var.pubkey}"
     zone = "{var.zone}"
     ssh_key = "{var.ssh_key}"
     redhat = "${var.redhat}"
     privsubnet = "${aws_subnet.privsubnet.id}"
     nsg = "${aws_security_group.apopx_nsg.id}"
}

