variable "rg_location" {
  default = "ukwest"
}

variable "username" {
  default = "scerry"
}

variable "ssh_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDONp5xX5QVZu0HQtqyNDUrs7cl7e4STEfJNPu7dA2UYbz2zuOwBLV4R0qYJUIrW8sOGRDInjPOJdhNXkXLNaPllYnUTRLzaeK40fWCl9x8QIyUUvvGIjXL+Z5g4t49NXvbux4tD0LX/0DQO/AT0mzQ4A8iwpNaEqPAScLb+w+64vU71nBj9mHci4CTl9xOOPY/27EDPetq4+rdrR2LiQQ3G0x1j0bQCeUHgfqVt7LGWvCl+VJxnk/WDAXuiqFUnCWP71vG+H4787gbDwH+iMz+y7/8xYc0siTyKjyhiJkqTFAf3v0KMb91z2F7mRSTbQH78kl2nJikdSm4NsfA3BAp"
}

variable "common_tags" {
  type = "map"
  default = {
    source = "Terraform"
    cloud = "Apopx-Cloud"
  }
}

variable "fe_prefix" {
  default = "Front-End"
}

variable "be_prefix" {
  default = "Back-End"
}

variable "cloud" {
  default = "Apopx-Cloud"
}
