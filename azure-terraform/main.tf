#terraform {
#  backend "azurerm" {
#    storage_account_name = "festrg"
#    container_name       = "terraform"
#    key                  = "XXXXXX"
#    access_key           = "XXXXXX"
#  }
#}

module "Front-End" {
  source   = "./modules/Front-End"
  prefix   = "${var.fe_prefix}"
  resource_group_location = "${var.rg_location}"
  common_tags = "${var.common_tags}"
  ssh_key = "${var.ssh_key}"
  username = "${var.username}"
  cloud = "${var.cloud}"
}

module "Back-End" {
  source   = "./modules/Back-End"
  prefix   = "${var.be_prefix}"
  resource_group_location = "${var.rg_location}"
  common_tags = "${var.common_tags}"
  ssh_key = "${var.ssh_key}"
  username = "${var.username}"
  cloud = "${var.cloud}"
}

resource "azurerm_virtual_network_peering" "peer1" {
  name                         = "peer1_to_peer2"
  resource_group_name          = "${var.fe_prefix}-${var.cloud}"
  virtual_network_name         = "${module.Front-End.fe_vn_name}"
  remote_virtual_network_id    = "${module.Back-End.be_vn_id}"
  allow_virtual_network_access = true
  allow_forwarded_traffic      = false
  allow_gateway_transit        = false
}

resource "azurerm_virtual_network_peering" "peer2" {
  name                         = "peer2_to_peer1"
  resource_group_name          = "${var.be_prefix}-${var.cloud}"
  virtual_network_name         = "${module.Back-End.be_vn_name}"
  remote_virtual_network_id    = "${module.Front-End.fe_vn_id}"
  allow_virtual_network_access = true
  allow_forwarded_traffic      = false
  allow_gateway_transit        = false
}

