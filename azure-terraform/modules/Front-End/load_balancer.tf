resource "azurerm_public_ip" "fe_lb_ip" {
  name                         = "${var.prefix}-pubip-rproxy"
  location                     = "${var.resource_group_location}"
  resource_group_name          = "${azurerm_resource_group.azure_rg.name}"
  public_ip_address_allocation = "static"

}

resource "azurerm_lb" "fe_lb" {
  name                = "${var.prefix}-lb-rproxy"
  resource_group_name = "${azurerm_resource_group.azure_rg.name}"
  location            = "${var.resource_group_location}"


  frontend_ip_configuration {
    name                          = "${var.prefix}-LBFrontEnd"
    public_ip_address_id            = "${azurerm_public_ip.fe_lb_ip.id}"
}
}

resource "azurerm_lb_backend_address_pool" "fe_lb_be" {
  resource_group_name = "${azurerm_resource_group.azure_rg.name}"
  loadbalancer_id     = "${azurerm_lb.fe_lb.id}"
  name                = "${var.prefix}-BackEndAddressPool"

}

resource "azurerm_lb_probe" "fe_lb_probe_ssh" {
  resource_group_name = "${azurerm_resource_group.azure_rg.name}"
  loadbalancer_id     = "${azurerm_lb.fe_lb.id}"
  name                = "ssh-running-probe"
  port                = "22"

}

resource "azurerm_lb_probe" "fe_lb_probe_https" {
  resource_group_name = "${azurerm_resource_group.azure_rg.name}"
  loadbalancer_id     = "${azurerm_lb.fe_lb.id}"
  name                = "https-running-probe"
  port                = "443"

}

resource "azurerm_lb_probe" "fe_lb_probe_http" {
  resource_group_name = "${azurerm_resource_group.azure_rg.name}"
  loadbalancer_id     = "${azurerm_lb.fe_lb.id}"
  name                = "http-running-probe"
  port                = "80"

}


resource "azurerm_lb_rule" "fe_lb_rule_http" {
  resource_group_name            = "${azurerm_resource_group.azure_rg.name}"
  loadbalancer_id                = "${azurerm_lb.fe_lb.id}"
  name                           = "LBRule_http"
  protocol                       = "Tcp"
  frontend_port                  = "80"
  backend_port                   = "80"
  frontend_ip_configuration_name = "${var.prefix}-LBFrontEnd"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.fe_lb_be.id}"
  enable_floating_ip             = false
  depends_on                     = ["azurerm_lb_probe.fe_lb_probe_http"]
  probe_id                       = "${azurerm_lb_probe.fe_lb_probe_http.id}"

}

resource "azurerm_lb_rule" "fe_lb_rule_https" {
  resource_group_name            = "${azurerm_resource_group.azure_rg.name}"
  loadbalancer_id                = "${azurerm_lb.fe_lb.id}"
  name                           = "LBRule_https"
  protocol                       = "Tcp"
  frontend_port                  = "443"
  backend_port                   = "443"
  frontend_ip_configuration_name = "${var.prefix}-LBFrontEnd"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.fe_lb_be.id}"
  enable_floating_ip             = false
  depends_on                     = ["azurerm_lb_probe.fe_lb_probe_https"]
  probe_id                       = "${azurerm_lb_probe.fe_lb_probe_https.id}"

}

resource "azurerm_lb_rule" "fe_lb_rule_ssh" {
  resource_group_name            = "${azurerm_resource_group.azure_rg.name}"
  loadbalancer_id                = "${azurerm_lb.fe_lb.id}"
  name                           = "LBRule_ssh"
  protocol                       = "Tcp"
  frontend_port                  = "22"
  backend_port                   = "22"
  frontend_ip_configuration_name = "${var.prefix}-LBFrontEnd"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.fe_lb_be.id}"
  enable_floating_ip             = false
  depends_on                     = ["azurerm_lb_probe.fe_lb_probe_ssh"]
  probe_id                       = "${azurerm_lb_probe.fe_lb_probe_ssh.id}"

}

