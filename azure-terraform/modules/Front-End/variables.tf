variable "prefix" {}
variable "ssh_key" {}
variable "resource_group_location" {}
variable "common_tags" { type = "map" }
variable "username" {}
variable "cloud" {}

variable "vnet_name" {
  default = "vnet-main" 
}

variable "subnet_main" {
  default = "subnet-main"
}
 
