variable "nsg_id" {}
variable "stacc_name" {}
variable "prefix" {}
variable "rg_name" {}
variable "rg_location" {}
variable "common_tags" { type = "map" }
variable "subnet_main" {}
variable "username" {}
variable "ssh" {}
variable "avail_set" {}
variable "lb_be_ip" {}
variable "vm_size_rproxy" {
   default = "Standard_B1s"
}

variable "os_name" {
   default = "Centos 7.5"
}

