# Create network interface - rproxy02
resource "azurerm_network_interface" "rproxy-2_nic" {
  name                      = "${var.prefix}-nic0-rproxy-2"
  location                  = "${var.rg_location}"
  resource_group_name       = "${var.rg_name}"
  network_security_group_id = "${var.nsg_id}"

  ip_configuration {
   name                          = "${var.prefix}-ipconf0-rproxy-2"
    subnet_id                     = "${var.subnet_main}"
    private_ip_address_allocation = "static"
    private_ip_address           = "10.1.0.5"
    load_balancer_backend_address_pools_ids = ["${var.lb_be_ip}"]
}
tags = "${merge(
    var.common_tags,
    map(
      "VM", "${var.prefix}",
      "OS", "${var.os_name}",
    )
  )}"
}

