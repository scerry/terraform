# Create virtual machine - rproxy02
resource "azurerm_virtual_machine" "rproxy2-vm" {
  name                  = "${var.prefix}-vm-rproxy-2"
  location              = "${var.rg_location}"
  resource_group_name   = "${var.rg_name}"
  network_interface_ids = ["${azurerm_network_interface.rproxy-2_nic.id}"]
  availability_set_id   = "${var.avail_set}"
  vm_size               = "${var.vm_size_rproxy}"

  storage_os_disk {
    name              = "${var.prefix}-osdisk-rproxy-2"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
  
  os_profile {
    computer_name = "${var.prefix}-vm-rproxy-2"
    admin_username = "${var.username}"
}

  os_profile_linux_config {
  disable_password_authentication = true

    ssh_keys = [{
      path     = "/home/scerry/.ssh/authorized_keys"
      key_data = "${var.ssh}"
    }]
  }
boot_diagnostics {
    enabled = "true"
    storage_uri = "${var.stacc_name}"
  }
tags = "${merge(
    var.common_tags,
    map(
      "VM", "rproxy-2",
      "OS", "${var.os_name}",
    )
  )}"

}

