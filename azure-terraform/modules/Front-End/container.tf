resource "azurerm_storage_container" "test" {
  name                  = "terraform"
  resource_group_name   = "${azurerm_resource_group.azure_rg.name}"
  storage_account_name  = "${azurerm_storage_account.azure_stacc.name}"
  container_access_type = "private"
}


