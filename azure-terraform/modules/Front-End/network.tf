# Create virtual network for Jenkins, Artifactory, Gitlab
resource "azurerm_virtual_network" "fe_vn" {
  name                = "${var.prefix}-${var.vnet_name}"
  address_space       = ["10.1.0.0/29"]
  location            = "${var.resource_group_location}"
  resource_group_name = "${azurerm_resource_group.azure_rg.name}"

tags = "${merge(
    var.common_tags,
    map(
      "Environment", "${var.prefix}",
    )
  )}"
}


# Create subnet
resource "azurerm_subnet" "fe_subnet_main" {
  name                 = "${var.prefix}-${var.subnet_main}"
  resource_group_name  = "${azurerm_resource_group.azure_rg.name}"
  virtual_network_name = "${azurerm_virtual_network.fe_vn.name}"
  address_prefix       = "10.1.0.0/29"
}

