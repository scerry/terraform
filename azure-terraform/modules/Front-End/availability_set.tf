resource "azurerm_availability_set" "fe-as" {
 name = "${var.prefix}-rproxy-Availability-Set"
 location = "${var.resource_group_location}"
 resource_group_name = "${azurerm_resource_group.azure_rg.name}"
 platform_fault_domain_count = "2"
 platform_update_domain_count = "2"
 managed = true
}
