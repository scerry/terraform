output "fe_vn_id" {
  value = "${azurerm_virtual_network.fe_vn.id}"
}

output "fe_vn_name" {
  value = "${azurerm_virtual_network.fe_vn.name}"
}

