#Load and configure rproxy01 server     
module "rproxy-1" {
        source = "./servers/rproxy-1"
        prefix = "${var.prefix}"

        nsg_id = "${azurerm_network_security_group.azure_nsg.id}"
	subnet_main = "${azurerm_subnet.fe_subnet_main.id}"
        stacc_name = "${azurerm_storage_account.azure_stacc.primary_blob_endpoint}"
        rg_name = "${azurerm_resource_group.azure_rg.name}"
        rg_location = "${var.resource_group_location}"
        common_tags = "${var.common_tags}"
	username = "${var.username}"
        ssh = "${var.ssh_key}"
	avail_set = "${azurerm_availability_set.fe-as.id}"
	lb_be_ip = "${azurerm_lb_backend_address_pool.fe_lb_be.id}"
}

#Load and configure rproxy02 server     
module "rproxy-2" {
        source = "./servers/rproxy-2"
 	prefix = "${var.prefix}"

	nsg_id = "${azurerm_network_security_group.azure_nsg.id}"
	subnet_main = "${azurerm_subnet.fe_subnet_main.id}"
       stacc_name = "${azurerm_storage_account.azure_stacc.primary_blob_endpoint}"
        rg_name = "${azurerm_resource_group.azure_rg.name}"
        rg_location = "${var.resource_group_location}"
        common_tags = "${var.common_tags}"
	username = "${var.username}"
        ssh = "${var.ssh_key}"
        avail_set = "${azurerm_availability_set.fe-as.id}"
	lb_be_ip = "${azurerm_lb_backend_address_pool.fe_lb_be.id}"
}

