# Create virtual machine - devserver
resource "azurerm_virtual_machine" "devserver-vm" {
  name                  = "${var.prefix}-vm-devserver"
  location              = "${var.rg_location}"
  resource_group_name   = "${var.rg_name}"
  network_interface_ids = ["${azurerm_network_interface.devserver_nic.id}"]
  vm_size               = "${var.vm_size_devserver}"

  storage_os_disk {
    name              = "${var.prefix}-osdisk-devserver"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
  
  os_profile {
    computer_name = "${var.prefix}-vm-devserver"
    admin_username = "${var.username}"
}

  os_profile_linux_config {
  disable_password_authentication = true

    ssh_keys = [{
      path     = "/home/scerry/.ssh/authorized_keys"
      key_data = "${var.ssh}"
    }]
  }
boot_diagnostics {
    enabled = "true"
    storage_uri = "${var.stacc_name}"
  }
tags = "${merge(
    var.common_tags,
    map(
      "VM", "devserver",
      "OS", "${var.os_name}",
      "Services", "Artifactory & GitLab",
    )
  )}"

}

