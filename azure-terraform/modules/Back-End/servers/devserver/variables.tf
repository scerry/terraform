variable "nsg_id" {}
variable "stacc_name" {}
variable "prefix" {}
variable "rg_name" {}
variable "rg_location" {}
variable "common_tags" { type = "map" }
variable "subnet_main" {}
variable "username" {}
variable "ssh" {}
variable "vm_size_devserver" {
   default = "Standard_B2ms"
}

variable "os_name" {
   default = "Centos 7.5"
}

