variable "nsg_id" {}
variable "stacc_name" {}
variable "prefix" {}
variable "rg_name" {}
variable "rg_location" {}
variable "common_tags" { type = "map" }
variable "subnet_main" {}
variable "username" {}
variable "ssh" {}

variable "vm_size_ldapmaster" {
   default = "Standard_B1s"
}

variable "os_name" {
   default = "Centos 7.5"
}

