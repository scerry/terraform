# Create network interface - jenkinsmaster
resource "azurerm_network_interface" "ldapmaster_nic" {
  name                      = "${var.prefix}-nic0-ldapmaster"
  location                  = "${var.rg_location}"
  resource_group_name       = "${var.rg_name}"
  network_security_group_id = "${var.nsg_id}"

  ip_configuration {
   name                          = "${var.prefix}-ipconf0-ldapmaster"
    subnet_id                     = "${var.subnet_main}"
    private_ip_address           = "10.2.0.4"
    private_ip_address_allocation = "static"
  }
tags = "${merge(
    var.common_tags,
    map(
      "VM", "${var.prefix}",
      "OS", "${var.os_name}",
    )
  )}"

}

