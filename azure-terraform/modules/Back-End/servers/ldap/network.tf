# Create network interface - jenkinsmaster
resource "azurerm_network_interface" "ldap_nic" {
  name                      = "${var.prefix}-nic0-ldap"
  location                  = "${var.rg_location}"
  resource_group_name       = "${var.rg_name}"
  network_security_group_id = "${var.nsg_id}"

  ip_configuration {
   name                          = "${var.prefix}-ipconf0-ldap"
    subnet_id                     = "${var.subnet_main}"
    private_ip_address           = "10.2.0.6"
    private_ip_address_allocation = "static"
  }
tags = "${merge(
    var.common_tags,
    map(
      "VM", "${var.prefix}",
      "OS", "${var.os_name}",
    )
  )}"

}

