# Create a resource group if it doesn’t exist
resource "azurerm_resource_group" "azure_rg" {
  name     = "${var.prefix}-${var.cloud}"
  location = "${var.resource_group_location}"

tags = "${merge(
    var.common_tags,
    map(
      "Environment", "${var.prefix}",
    )
  )}"
}

