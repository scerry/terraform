# Create storage account for boot diagnostics
resource "azurerm_storage_account" "azure_stacc" {
  name                        = "bestrg"
  resource_group_name         = "${azurerm_resource_group.azure_rg.name}"
  location                    = "${var.resource_group_location}"
  account_tier                = "Standard"
  account_replication_type    = "LRS"
tags = "${merge(
    var.common_tags,
    map(
      "Environment", "${var.prefix}",
    )
  )}"
}

