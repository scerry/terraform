output "be_vn_id" {
  value = "${azurerm_virtual_network.be_vn.id}"
}

output "be_vn_name" {
  value = "${azurerm_virtual_network.be_vn.name}"
}

