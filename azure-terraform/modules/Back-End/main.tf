#Deploy Jenkins Server
module "ldapmaster" {
	source = "./servers/ldapmaster"
        prefix = "${var.prefix}"
 
        nsg_id = "${azurerm_network_security_group.azure_nsg.id}"
        subnet_main = "${azurerm_subnet.be_subnet_main.id}" 
	stacc_name = "${azurerm_storage_account.azure_stacc.primary_blob_endpoint}"
	rg_name = "${azurerm_resource_group.azure_rg.name}"
	rg_location = "${var.resource_group_location}"
	common_tags = "${var.common_tags}"
	username = "${var.username}"
        ssh = "${var.ssh_key}"
}

#Create devserver Server
module "devserver" {
        source = "./servers/devserver"
	prefix = "${var.prefix}"

        nsg_id = "${azurerm_network_security_group.azure_nsg.id}"
	subnet_main = "${azurerm_subnet.be_subnet_main.id}"
        stacc_name = "${azurerm_storage_account.azure_stacc.primary_blob_endpoint}"
        rg_name = "${azurerm_resource_group.azure_rg.name}"
        rg_location = "${var.resource_group_location}"
        common_tags = "${var.common_tags}"
	username = "${var.username}"
        ssh = "${var.ssh_key}"
}

#module "ldapm" {
#        source = "./servers/ldap"
#        prefix = "${var.prefix}"
#
#        nsg_id = "${azurerm_network_security_group.azure_nsg.id}"
#        subnet_main = "${azurerm_subnet.be_subnet_main.id}"
#        stacc_name = "${azurerm_storage_account.azure_stacc.primary_blob_endpoint}"
#        rg_name = "${azurerm_resource_group.azure_rg.name}"
#        rg_location = "${var.resource_group_location}"
#        common_tags = "${var.common_tags}"
#        username = "${var.username}"
#        ssh = "${var.ssh_key}"
#}

